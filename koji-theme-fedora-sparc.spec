%define baserelease 2
#build with --define 'testbuild 1' to have a timestamp appended to release
%if 0%{?testbuild}
%define release %{baserelease}.%(date +%%Y%%m%%d.%%H%%M.%%S)
%else
%define release %{baserelease}
%endif
Name: koji-theme-fedora-sparc
Version: 1.3
Release: %{release}%{?dist}
License: GPLv2
Summary: Fedora SPARC koji theme
Group: Applications/Internet
Source: %{name}-%{PACKAGE_VERSION}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: koji-web

%description
Makes the fedora SPARC koji web ui unique

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc Authors COPYING README
%{_datadir}/koji-themes/fedora-sparc
%config(noreplace) /etc/httpd/conf.d/00kojifedorasparc.conf


%changelog
* Fri Jul 31 2009 Dennis Gilmore <dennis@ausil.us> - 1.3-1
-initial build
